﻿using System;

namespace ProyectoConversion.Models
{
    public class ArchivoPdf
    {
        public string NombreArchivo { get; set; }
        public byte[] Contenido { get; set; }
    }
}