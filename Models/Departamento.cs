﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProyectoConversion.Models
{
    public class Departamento
    {
        public long Id { get; set; }
        public DateTime AltaFecha { get; set; }
        public string AltaUsuario { get; set; }
        public DateTime? ModificacionFecha { get; set; }
        public string ModificacionUsuario { get; set; }
        public DateTime? BajaFecha { get; set; }
        public string BajaUsuario { get; set; }
        public bool Activo { get; set; }
        [Required(ErrorMessage = "Campo obligatorio")]
        public string Nombre { get; set; }
        public string Comentarios { get; set; }
        [Required(ErrorMessage = "Campo obligatorio")]
        public long? UltimaMatricula { get; set; }
        [Required(ErrorMessage = "Campo obligatorio")]
        public string Codigo { get; set; }
    }
}