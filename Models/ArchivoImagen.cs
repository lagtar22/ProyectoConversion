﻿using System;

namespace ProyectoConversion.Models
{
    public class ArchivoImagen //Para Imagen de Matrícula
    {
        public ArchivoPdf ArchivoPdf { get; set; }
        public ImagenesMatriculas ImagenMatricula { get; set; }
        public string NombreDepartamento { get; set; }
    }/*
    public class ArchivoImagen //Para Imagen de Tomo Cronológico
    {
        public ArchivoPdf ArchivoPdf { get; set; }
        public ImagenesMatriculas ImagenTomo { get; set; }
        public string Departamento { get; set; }
    }
    public class ArchivoImagen //Para Imagen de Minuta
    {
        public ArchivoPdf ArchivoPdf { get; set; }
        public ImagenesMatriculas ImagenMinuta { get; set; }
    }*/
}