﻿using System;

namespace ProyectoConversion.Models
{
    public class ImagenesMatriculas
    {
        public long Id { get; set; }
        public DateTime AltaFecha { get; set; }
        public string AltaUsuario { get; set; }
        public DateTime? ModificacionFecha { get; set; }
        public string ModificacionUsuario { get; set; }
        public DateTime? BajaFecha { get; set; }
        public string BajaUsuario { get; set; }
        public bool Activo { get; set; }
        public string Nombre { get; set; }
        public string Ubicacion { get; set; }
        public string Departamento { get; set; }
        public string Matricula { get; set; }
        public long? Digito { get; set; }
        public string Duplicado { get; set; }
        public DateTime Fecha { get; set; }
        public string Extension { get; set; }
        public string Size { get; set; }
        public long Version { get; set; }
    }
}