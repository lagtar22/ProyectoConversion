#Depending on the operating system of the host machines(s) that will build or run the containers, the image specified in the FROM statement may need to be changed.
#For more information, please see https://aka.ms/containercompat

FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src
COPY ["ProyectoConversion.csproj", "."]
RUN dotnet restore "./ProyectoConversion.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "ProyectoConversion.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "ProyectoConversion.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "ProyectoConversion.dll"]