﻿using System.Web;
using System.Web.Mvc;

namespace ProyectoConversion
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            // Añadir el filtro de error
            filters.Add(new HandleErrorAttribute());
        }
    }
}
