﻿using System.Web.Mvc;

namespace ProyectoConversion.Controllers
{
    [Authorize]
    public class InicioController : Controller
    {
        // GET: Inicio        
        public ActionResult Inicio()
        {
            return View();
        }
    }
}