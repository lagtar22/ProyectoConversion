﻿using ProyectoConversion.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using PagedList;
using System.Web;
using Aspose.Pdf;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Minio.DataModel.Args;
using Minio.Exceptions;
using Minio;

namespace ProyectoConversion.Controllers
{
    [Authorize]
    public class ImagenesMatriculasController : Controller
    {
        private static readonly string conexionSIR = ConfigurationManager.ConnectionStrings["AppDbContext"].ToString();
        private static List<ImagenesMatriculas> oImagenesMatriculas = new List<ImagenesMatriculas>();
        private readonly string bucketMatriculas;
        private readonly string bucketHistorcos;
        public ImagenesMatriculasController()
        {
            bucketMatriculas = ConfigurationManager.AppSettings["BucketMatriculasVigentes"];
            bucketHistorcos = ConfigurationManager.AppSettings["BucketMatriculasHistoricas"];
        }
        // GET: ImagenesMatriculas
        [HttpGet]
        public async Task<ActionResult> Activos(string matricula, int? page)
        {
            int pageSize = 30;
            int pageNumber = (page ?? 0) == 0 ? 1 : page.Value;
            oImagenesMatriculas = new List<ImagenesMatriculas>();
            using (SqlConnection oConexion = new SqlConnection(conexionSIR))
            {
                string query =  "SELECT * FROM ImagenesMatriculas " +
                                    "WHERE Activo = 1 ";
                if (!string.IsNullOrEmpty(matricula))
                {
                    query +=        "AND Nombre LIKE @Busqueda ";
                }
                // Modificación para ordenamiento y paginación
                query +=            "ORDER BY Departamento ASC, TRY_CAST(Matricula AS INT) ASC, Digito ASC, Duplicado ASC " +
                                    "OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY";
                SqlCommand cmd = new SqlCommand(query, oConexion)
                {
                    CommandType = CommandType.Text
                };
                if (!string.IsNullOrEmpty(matricula))
                {
                    cmd.Parameters.AddWithValue("@Busqueda", "%" + matricula + "%");
                }
                // Añadir parámetros de paginación
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@PageNumber", pageNumber);
                await oConexion.OpenAsync();
                using (SqlDataReader dr = await cmd.ExecuteReaderAsync())
                {
                    while (await dr.ReadAsync() && dr.HasRows)
                    {
                        ImagenesMatriculas nuevaImagenMatricula = new ImagenesMatriculas
                        {
                            Id = Convert.ToInt64(dr["Id"]),
                            AltaFecha = Convert.ToDateTime(dr["AltaFecha"]),
                            AltaUsuario = dr["AltaUsuario"] != DBNull.Value ? dr["AltaUsuario"].ToString() : null,
                            ModificacionFecha = dr["ModificacionFecha"] != DBNull.Value ? (DateTime?)dr["ModificacionFecha"] : null,
                            ModificacionUsuario = dr["ModificacionUsuario"] != DBNull.Value ? dr["ModificacionUsuario"].ToString() : null,
                            BajaFecha = dr["BajaFecha"] != DBNull.Value ? (DateTime?)dr["BajaFecha"] : null,
                            BajaUsuario = dr["BajaUsuario"] != DBNull.Value ? dr["BajaUsuario"].ToString() : null,
                            Activo = Convert.ToBoolean(dr["Activo"]),
                            Nombre = dr["Nombre"].ToString(),
                            Ubicacion = dr["Ubicacion"].ToString(),
                            Departamento = dr["Departamento"].ToString(),
                            Matricula = dr["Matricula"].ToString(),
                            Digito = dr["Digito"] != DBNull.Value ? (long?)dr["Digito"] : null,
                            Duplicado = dr["Duplicado"] != DBNull.Value ? dr["Duplicado"].ToString() : null,
                            Fecha = Convert.ToDateTime(dr["Fecha"]),
                            Extension = dr["Extension"].ToString(),
                            Size = dr["Size"].ToString(),
                            Version = Convert.ToInt64(dr["Version"]),
                        };
                        oImagenesMatriculas.Add(nuevaImagenMatricula);
                    }
                }
            }
            int totalItemCount = await TotalMatriculas(matricula); // Obtener el total de elementos sin paginar
            var pagedImagenesMatriculas = new StaticPagedList<ImagenesMatriculas>(oImagenesMatriculas, pageNumber, pageSize, totalItemCount);
            ViewBag.SearchTerm = matricula;
            return View(pagedImagenesMatriculas);
        }
        [HttpGet]
        public async Task<ActionResult> Editadas(int? page)
        {
            int pageSize = 30;
            int pageNumber = (page ?? 0) == 0 ? 1 : page.Value;
            oImagenesMatriculas = new List<ImagenesMatriculas>();
            using (SqlConnection oConexion = new SqlConnection(conexionSIR))
            {
                string query =  "SELECT * FROM ImagenesMatriculas " +
                                    "WHERE Version != 1 " +
                                    "ORDER BY Departamento ASC, TRY_CAST(Matricula AS INT) ASC, Digito ASC, Duplicado ASC " +
                                    "OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY";
                SqlCommand cmd = new SqlCommand(query, oConexion)
                {
                    CommandType = CommandType.Text
                };
                // Añadir parámetros de paginación
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@PageNumber", pageNumber);
                await oConexion.OpenAsync();
                using (SqlDataReader dr = await cmd.ExecuteReaderAsync())
                {
                    while (await dr.ReadAsync() && dr.HasRows)
                    {
                        ImagenesMatriculas nuevaImagenMatricula = new ImagenesMatriculas
                        {
                            Id = Convert.ToInt64(dr["Id"]),
                            AltaFecha = Convert.ToDateTime(dr["AltaFecha"]),
                            AltaUsuario = dr["AltaUsuario"] != DBNull.Value ? dr["AltaUsuario"].ToString() : null,
                            ModificacionFecha = dr["ModificacionFecha"] != DBNull.Value ? (DateTime?)dr["ModificacionFecha"] : null,
                            ModificacionUsuario = dr["ModificacionUsuario"] != DBNull.Value ? dr["ModificacionUsuario"].ToString() : null,
                            BajaFecha = dr["BajaFecha"] != DBNull.Value ? (DateTime?)dr["BajaFecha"] : null,
                            BajaUsuario = dr["BajaUsuario"] != DBNull.Value ? dr["BajaUsuario"].ToString() : null,
                            Activo = Convert.ToBoolean(dr["Activo"]),
                            Nombre = dr["Nombre"].ToString(),
                            Ubicacion = dr["Ubicacion"].ToString(),
                            Departamento = dr["Departamento"].ToString(),
                            Matricula = dr["Matricula"].ToString(),
                            Digito = dr["Digito"] != DBNull.Value ? (long?)dr["Digito"] : null,
                            Duplicado = dr["Duplicado"] != DBNull.Value ? dr["Duplicado"].ToString() : null,
                            Fecha = Convert.ToDateTime(dr["Fecha"]),
                            Extension = dr["Extension"].ToString(),
                            Size = dr["Size"].ToString(),
                            Version = Convert.ToInt64(dr["Version"]),
                        };
                        oImagenesMatriculas.Add(nuevaImagenMatricula);
                    }
                }
            }
            int totalItemCount = await TotalMatriculasConVersion(""); // Obtener el total de elementos sin paginar
            var pagedImagenesMatriculas = new StaticPagedList<ImagenesMatriculas>(oImagenesMatriculas, pageNumber, pageSize, totalItemCount);
            return View(pagedImagenesMatriculas);
        }
        [HttpGet]
        public async Task<ActionResult> Precarga()
        {
            List<string> departamentos = await ObtenerDepartamentos();
            if (departamentos.Count > 0)
            {
                return View(departamentos);
            }
            else
            {
                TempData["error"] = "No se pudieron cargar los Departamentos.";
                return RedirectToAction("Activos", "ImagenesMatriculas");
            }
        }
        [HttpGet]
        public async Task<ActionResult> Descargar(long? idImgMat)
        {
            if (idImgMat == null)
            {
                TempData["error"] = "ID nulo.";
                return RedirectToAction("Activos", "ImagenesMatriculas");
            }
            ImagenesMatriculas oImagenMatricula = await Task.Run(() => oImagenesMatriculas.Where(d => d.Id == idImgMat).FirstOrDefault());
            if (oImagenMatricula == null)
            {
                TempData["error"] = "La Imagen Matricula no fue encontrada.";
                return RedirectToAction("Activos", "ImagenesMatriculas");
            }
            return RedirectToAction("Descargar", "Minio", oImagenMatricula);
        }
        [HttpGet]
        public async Task<ActionResult> Editar(long? idImgMat)
        {
            if (idImgMat == null)
            {
                if (TempData["IdImgMat"] != null)
                {
                    idImgMat = Convert.ToInt64(TempData["IdImgMat"]);
                }
            }
            if (idImgMat == null)
            {
                TempData["error"] = "ID nulo.";
                return RedirectToAction("Activos", "ImagenesMatriculas");
            }
            ImagenesMatriculas imagen = await ObtenerImagenMatricula((long)idImgMat);
            if (!await ExisteArchivoMinio(imagen.Nombre)) // Existe archivo en minio
            {
                TempData["redireccion"] = "Archivo no registrado, proceda a subir.";
                return RedirectToAction("Precarga", "ImagenesMatriculas");
            }
            return View(idImgMat);
        }
        //Métodos post
        [HttpPost]
        public async Task<ActionResult> Comprobar(string departamento, string matricula, string digito, string duplicado, HttpPostedFileBase archivo)
        {
            try
            {
                // Verificar que el archivo no sea nulo, tenga contenido y no sea un PDF
                if (archivo == null || archivo.ContentLength == 0 || Path.GetExtension(archivo.FileName)?.ToLower() != ".pdf")
                {
                    TempData["error"] = "El archivo no es un PDF válido.";
                    return RedirectToAction("Precarga", "ImagenesMatriculas");
                }
                if (archivo.ContentLength > 100*1024*1024)
                {
                    TempData["exceso"] = "El archivo no debe superar los 100MB";
                    return RedirectToAction("Precarga", "ImagenesMatriculas");
                }
                if (string.IsNullOrEmpty(departamento))
                {
                    TempData["error"] = "Debe ingresar un departamento.";
                    return RedirectToAction("Precarga", "ImagenesMatriculas");
                }
                if (string.IsNullOrEmpty(matricula))
                {
                    TempData["error"] = "Debe ingresar una matrícula.";
                    return RedirectToAction("Precarga", "ImagenesMatriculas");
                }
                // Comprobar si existe la inscripción de la matrícula en ese departamento
                int exMat = await ExisteInscripcionInmueble(departamento, matricula);
                if (exMat == 0) // Inscripción no encontrada, mostrar mensaje de error
                {
                    TempData["error"] = "Inscripción no encontrada para la matrícula en ese departamento.";
                    return RedirectToAction("Precarga", "ImagenesMatriculas");
                }
                int exObj = await ExisteObjetoFisico(departamento, matricula);
                if (exObj == 0) // Objeto Fisico no encontrado, mostrar mensaje de error
                {                    
                    TempData["error"] = "Objeto Fisico no encontrado para la matrícula en ese departamento.";
                    return RedirectToAction("Precarga", "ImagenesMatriculas");
                }
                string codDepto = await CodigoDepartamento(departamento);
                string nombre = await NuevoNombre(codDepto, matricula, digito, duplicado);
                // Convertir el contenido del archivo PDF a bytes
                byte[] contenidoArchivoPDF;
                using (var memoryStream = new MemoryStream())
                {
                    archivo.InputStream.CopyTo(memoryStream);
                    contenidoArchivoPDF = memoryStream.ToArray();
                }
                // Crear el objeto ArchivoPdf
                ArchivoPdf archivoPdf = new ArchivoPdf
                {
                    NombreArchivo = nombre,
                    Contenido = contenidoArchivoPDF
                };
                // Crear el objeto ImagenesMatriculas
                ImagenesMatriculas nuevaImagenMatricula = new ImagenesMatriculas
                {
                    Nombre = nombre,
                    Departamento = codDepto,
                    Matricula = matricula,
                    Digito = string.IsNullOrEmpty(digito) ? (long?)null : Convert.ToInt64(digito),
                    Duplicado = duplicado,
                };
                // Leer la fecha de creación del archivo PDF
                using (var pdfDocument = new Document(archivo.InputStream))
                {
                    var info = pdfDocument.Info;
                    nuevaImagenMatricula.Fecha = info.CreationDate != null ? info.CreationDate : DateTime.Now;
                    nuevaImagenMatricula.Extension = "pdf";
                    nuevaImagenMatricula.Size = ((float)archivo.InputStream.Length / (1024 * 1024)).ToString("0.00") + " MB";
                }
                ArchivoImagen nuevoArchivoImagen = new ArchivoImagen
                {
                    ArchivoPdf = archivoPdf,
                    ImagenMatricula = nuevaImagenMatricula,
                    NombreDepartamento = departamento,
                };
                TempData["exito"] = "Inscripción encontrada!!";
                return View("Confirmacion", nuevoArchivoImagen);
            }
            catch (Exception ex)
            {
                TempData["error"] = "Error al procesar los datos: " + ex.Message;
                return RedirectToAction("Precarga");
            }
        }
        [HttpPost]
        public ActionResult Previsualizar(string nombreArchivo, string contenido)
        {
            // Verificar que el contenido del archivo no sea nulo o vacío
            if (string.IsNullOrEmpty(contenido))
            {
                return File(Encoding.UTF8.GetBytes("El archivo no está disponible."), "text/plain", "Error.txt");
            }
            byte[] contenidoBytes = Convert.FromBase64String(contenido);
            // Establecer el tipo de contenido para el PDF
            string contentType = "application/pdf";
            // Devolver el archivo como ActionResult
            Response.Headers.Add("Content-Disposition", $"inline; filename=\"{nombreArchivo}\"");
            return File(contenidoBytes, contentType);
        }
        [HttpPost]
        public async Task<ActionResult> Insertar(ArchivoImagen nuevoArchivoImagen)
        {
            if (nuevoArchivoImagen.ArchivoPdf == null || nuevoArchivoImagen.ImagenMatricula == null)
            {
                TempData["error"] = "La información de la Matrícula es nula.";
                return RedirectToAction("Precarga", "ImagenesMatriculas");
            }
            ImagenesMatriculas nuevaImagenMatricula = nuevoArchivoImagen.ImagenMatricula;
            // Comprobar si existe la inscripción de la matrícula en ese departamento
            int exMat = await ExisteInscripcionInmueble(nuevaImagenMatricula.Departamento, nuevaImagenMatricula.Matricula);
            if (exMat == 0) // Inscripción no encontrada, mostrar mensaje de error
            {
                TempData["error"] = "Inscripción no encontrada para la matrícula en ese departamento.";
                return RedirectToAction("Precarga", "ImagenesMatriculas");
            }
            int exObj = await ExisteObjetoFisico(nuevaImagenMatricula.Departamento, nuevaImagenMatricula.Matricula);
            if (exObj == 0) // Objeto Fisico no encontrado, mostrar mensaje de error
            {
                TempData["error"] = "Objeto Fisico no encontrado para la matrícula en ese departamento.";
                return RedirectToAction("Precarga", "ImagenesMatriculas");
            }
            Usuario usuario = HttpContext.Session["Usuario"] as Usuario;
            try
            {
                using (SqlConnection oConexion = new SqlConnection(conexionSIR))
                {
                    SqlCommand cmd = new SqlCommand("SP_InsertarImagenMatricula", oConexion)
                    {
                        CommandType = CommandType.StoredProcedure,
                    };     
                    cmd.Parameters.AddWithValue("AltaUsuario", usuario.User);
                    cmd.Parameters.AddWithValue("Nombre", nuevaImagenMatricula.Nombre);
                    cmd.Parameters.AddWithValue("Ubicacion", "matriculas");
                    cmd.Parameters.AddWithValue("Departamento", nuevaImagenMatricula.Departamento);
                    cmd.Parameters.AddWithValue("Matricula", nuevaImagenMatricula.Matricula);
                    cmd.Parameters.AddWithValue("Digito", nuevaImagenMatricula.Digito);
                    cmd.Parameters.AddWithValue("Duplicado", nuevaImagenMatricula.Duplicado);
                    cmd.Parameters.AddWithValue("Fecha", nuevaImagenMatricula.Fecha);
                    cmd.Parameters.AddWithValue("Extension", nuevaImagenMatricula.Extension);
                    cmd.Parameters.AddWithValue("Size", nuevaImagenMatricula.Size);
                    oConexion.Open();
                    cmd.ExecuteNonQuery();
                }
                TempData["ArchivoImagen"] = nuevoArchivoImagen;
                return RedirectToAction("Subir", "Minio");
            }
            catch
            {
                return View();
            }
        }
        [HttpPost]
        public async Task<ActionResult> Editar(long idEditar, HttpPostedFileBase archivo)
        {            
            // Verificar que el archivo no sea nulo, tenga contenido y no sea un PDF
            if (archivo == null || archivo.ContentLength == 0 || Path.GetExtension(archivo.FileName)?.ToLower() != ".pdf")
            {
                TempData["error"] = "El archivo no es un PDF válido.";
                return RedirectToAction("Activos", "ImagenesMatriculas");
            }
            ImagenesMatriculas imagenVieja = await ObtenerImagenMatricula(idEditar);
            // Comprobar si existe la inscripción de la matrícula en ese departamento
            int exMat = await ExisteInscripcionInmueble(imagenVieja.Departamento, imagenVieja.Matricula);
            if (exMat == 0) // Inscripción no encontrada, mostrar mensaje de error
            {
                TempData["error"] = "Inscripción no encontrada para la matrícula en ese departamento.";
                return RedirectToAction("Activos", "ImagenesMatriculas");
            }
            int exObj = await ExisteObjetoFisico(imagenVieja.Departamento, imagenVieja.Matricula);
            if (exObj == 0) // Objeto Fisico no encontrado, mostrar mensaje de error
            {
                TempData["error"] = "Objeto Fisico no encontrado para la matrícula en ese departamento.";
                return RedirectToAction("Activos", "ImagenesMatriculas");
            }
            Usuario usuario = HttpContext.Session["Usuario"] as Usuario;
            // Convertir el contenido del archivo PDF a bytes
            byte[] contenidoArchivoPDF;
            using (var memoryStream = new MemoryStream())
            {
                archivo.InputStream.CopyTo(memoryStream);
                contenidoArchivoPDF = memoryStream.ToArray();
            }
            using (var pdfDocument = new Document(archivo.InputStream))
            {
                var info = pdfDocument.Info;
                imagenVieja.Fecha = info.CreationDate != null ? info.CreationDate : DateTime.Now;
                imagenVieja.Size = ((float)archivo.InputStream.Length / (1024 * 1024)).ToString("0.00") + " MB";
            }
            ArchivoImagen archivoImagen = new ArchivoImagen()
            {
                ImagenMatricula = imagenVieja,
                ArchivoPdf = new ArchivoPdf()
                {
                    NombreArchivo = imagenVieja.Nombre,
                    Contenido = contenidoArchivoPDF,
                },
                NombreDepartamento = null,
            };
            try
            {
                using (SqlConnection oConexion = new SqlConnection(conexionSIR))
                {
                    SqlCommand cmd = new SqlCommand("SP_ModificarImagenMatricula", oConexion)
                    {
                        CommandType = CommandType.StoredProcedure,
                    };
                    cmd.Parameters.AddWithValue("Id", imagenVieja.Id);
                    cmd.Parameters.AddWithValue("ModificacionUsuario", usuario.User);
                    cmd.Parameters.AddWithValue("Fecha", imagenVieja.Fecha);
                    cmd.Parameters.AddWithValue("Size", imagenVieja.Size);
                    cmd.Parameters.AddWithValue("Version", imagenVieja.Version+1);
                    oConexion.Open();
                    cmd.ExecuteNonQuery();
                }
                TempData["ArchivoImagen"] = archivoImagen;
                return RedirectToAction("Editar", "Minio");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Ocurrió un error al editar el departamento: " + ex.Message);
                return View(idEditar);
            }
        }
        [HttpPost]
        public async Task<ActionResult> Alta(string nombreDepartamento, string nombre, string codigoDepartamento, string matricula, long? digito, string duplicado, DateTime fecha, string extension, string size, string nombreArchivo, string contenido)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    TempData["error"] = "Algo feo ocurrió. Consulte con su médico de cabecera.";
                    return RedirectToAction("Precarga", "ImagenesMatriculas");
                }
                int exImgMat = await ExisteImagenMatricula(codigoDepartamento, matricula, digito?.ToString(), duplicado);
                ArchivoImagen archivoImagen = new ArchivoImagen()
                {
                    ImagenMatricula = new ImagenesMatriculas()
                    {
                        Nombre = nombre,
                        Departamento = codigoDepartamento,
                        Matricula = matricula,
                        Digito = digito,
                        Duplicado = duplicado,
                        Fecha = fecha,
                        Extension = extension,
                        Size = size,
                    },
                    ArchivoPdf = new ArchivoPdf()
                    {
                        NombreArchivo = nombreArchivo,
                        Contenido = Convert.FromBase64String(contenido),
                    },
                    NombreDepartamento = nombreDepartamento,
                };
                if (exImgMat != 0) // Existe registro en bdd
                {
                    if (await ExisteArchivoMinio(archivoImagen.ImagenMatricula.Nombre)) // Existe archivo en minio
                    {                        
                        TempData["redireccion"] = "Imagen Matrícula ya registrada, proceda a edición.";
                        int IdImgMat = await ObtenerIdImagenMatricula(codigoDepartamento, matricula, digito?.ToString(), duplicado);
                        // Almacena el IdImgMat en TempData
                        TempData["IdImgMat"] = IdImgMat;
                        return RedirectToAction("Editar", "ImagenesMatriculas");
                    }
                    else // no existe archivo en minio
                    {
                        TempData["ArchivoImagen"] = archivoImagen;
                        return RedirectToAction("Subir", "Minio");
                    }
                }
                else // no existe registro en bdd
                {
                    return RedirectToAction("Insertar", "ImagenesMatriculas", archivoImagen);
                }
            }
            catch (Exception ex)
            {
                TempData["error"] = "Error al procesar los datos: " + ex.Message;
                return RedirectToAction("Precarga");
            }
        }
        [HttpPost]
        public ActionResult Buscar(string matricula)
        {
            return RedirectToAction("Activos", new { matricula });
        }
        //Métodos privados
        private async Task<int> TotalMatriculas(string matricula)
        {
            using (SqlConnection oConexion = new SqlConnection(conexionSIR))
            {
                string query = "SELECT COUNT(*) FROM ImagenesMatriculas WHERE Activo = 1 ";
                if (!string.IsNullOrEmpty(matricula))
                {
                    query += "AND Nombre LIKE @Busqueda";
                }
                SqlCommand cmd = new SqlCommand(query, oConexion)
                {
                    CommandType = CommandType.Text
                };
                if (!string.IsNullOrEmpty(matricula))
                {
                    cmd.Parameters.AddWithValue("@Busqueda", matricula + "%");
                }
                await oConexion.OpenAsync();
                return (int)await cmd.ExecuteScalarAsync();
            }
        }        
        private async Task<int> TotalMatriculasConVersion(string matricula)
        {
            using (SqlConnection oConexion = new SqlConnection(conexionSIR))
            {
                string query = "SELECT COUNT(*) FROM ImagenesMatriculas WHERE Version != 1 ";
                if (!string.IsNullOrEmpty(matricula))
                {
                    query += "AND Nombre LIKE @Busqueda";
                }
                SqlCommand cmd = new SqlCommand(query, oConexion)
                {
                    CommandType = CommandType.Text
                };
                if (!string.IsNullOrEmpty(matricula))
                {
                    cmd.Parameters.AddWithValue("@Busqueda", matricula + "%");
                }
                await oConexion.OpenAsync();
                return (int)await cmd.ExecuteScalarAsync();
            }
        }         
        private async Task<List<string>> ObtenerDepartamentos()
        {
            List<string> departamentos = new List<string>();
            using (SqlConnection conexion = new SqlConnection(conexionSIR))
            {
                await conexion.OpenAsync();
                using (SqlCommand comando = new SqlCommand("SELECT Departamentos FROM VW_NombresDepartamentos", conexion))
                {
                    using (SqlDataReader lector = await comando.ExecuteReaderAsync())
                    {
                        while (await lector.ReadAsync())
                        {
                            departamentos.Add(lector.GetString(0));
                        }
                    }
                }
            }
            return departamentos;
        }
        private async Task<int> ExisteInscripcionInmueble(string departamento, string matricula)
        {
            int existeMatricula = 0;
            try
            {
                using (SqlConnection conexion = new SqlConnection(conexionSIR))
                {
                    using (SqlCommand command = new SqlCommand("SP_ExisteMatricula", conexion))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@NombreDepartamento", departamento);
                        command.Parameters.AddWithValue("@Matricula", matricula);
                        await conexion.OpenAsync();
                        var result = await command.ExecuteScalarAsync();
                        if (result != null && result != DBNull.Value)
                        {
                            existeMatricula = Convert.ToInt32(result);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error al obtener la inscripción del inmueble: " + ex.Message);
            }
            return existeMatricula;
        }
        private async Task<int> ExisteObjetoFisico(string departamento, string matricula)
        {
            int existeObjeto = 0;
            try
            {
                using (SqlConnection conexion = new SqlConnection(conexionSIR))
                {
                    using (SqlCommand command = new SqlCommand("SP_ExisteObjetoFisico", conexion))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@NombreDepartamento", departamento);
                        command.Parameters.AddWithValue("@Matricula", matricula);
                        await conexion.OpenAsync();
                        var result = await command.ExecuteScalarAsync();
                        if (result != null && result != DBNull.Value)
                        {
                            existeObjeto = Convert.ToInt32(result);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error al obtener el objeto fisico: " + ex.Message);
            }
            return existeObjeto;
        }
        private async Task<int> ExisteImagenMatricula(string departamento, string matricula, string digito, string duplicado)
        {
            int existeImagenMatricula = 0;
            try
            {
                using (SqlConnection conexion = new SqlConnection(conexionSIR))
                {
                    using (SqlCommand command = new SqlCommand("SP_ExisteImagenMatricula", conexion))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Departamento", departamento);
                        command.Parameters.AddWithValue("@Matricula", matricula);
                        command.Parameters.AddWithValue("@Digito", string.IsNullOrEmpty(digito) ? (object)DBNull.Value : (object)digito);
                        command.Parameters.AddWithValue("@Duplicado", string.IsNullOrEmpty(duplicado) ? (object)DBNull.Value : (object)duplicado);
                        await conexion.OpenAsync();
                        var result = await command.ExecuteScalarAsync();
                        if (result != null && result != DBNull.Value)
                        {
                            existeImagenMatricula = Convert.ToInt32(result);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error al obtener el objeto fisico: " + ex.Message);
            }
            return existeImagenMatricula;
        }
        private async Task<int> ObtenerIdImagenMatricula(string departamento, string matricula, string digito, string duplicado)
        {
            int idImgMat = 0;
            try
            {
                using (SqlConnection conexion = new SqlConnection(conexionSIR))
                {
                    using (SqlCommand command = new SqlCommand("SP_ObtenerIdImagenMatricula", conexion))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Departamento", departamento);
                        command.Parameters.AddWithValue("@Matricula", matricula);
                        command.Parameters.AddWithValue("@Digito", string.IsNullOrEmpty(digito) ? (object)DBNull.Value : (object)digito);
                        command.Parameters.AddWithValue("@Duplicado", string.IsNullOrEmpty(duplicado) ? (object)DBNull.Value : (object)duplicado);
                        await conexion.OpenAsync();
                        var result = await command.ExecuteScalarAsync();
                        if (result != null && result != DBNull.Value)
                        {
                            idImgMat = Convert.ToInt32(result);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error al obtener el objeto fisico: " + ex.Message);
            }
            return idImgMat;
        }
        private async Task<string> CodigoDepartamento(string departamento)
        {
            string codigo = "";
            try
            {
                using (SqlConnection conexion = new SqlConnection(conexionSIR))
                {
                    using (SqlCommand command = new SqlCommand("SP_CodigoDepartamento", conexion))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@NombreDepartamento", departamento);
                        await conexion.OpenAsync();
                        var result = await command.ExecuteScalarAsync();
                        if (result != null && result != DBNull.Value)
                        {
                            codigo = result.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error al obtener el objeto fisico: " + ex.Message);
            }
            return codigo;
        }
        private async Task<bool> ExisteArchivoMinio(string nombreArchivo)
        {
            string minioEndpoint = ConfigurationManager.AppSettings["MinioEndpoint"];
            string minioAccessKey = ConfigurationManager.AppSettings["MinioAccessKey"];
            string minioSecretKey = ConfigurationManager.AppSettings["MinioSecretKey"];
            string minioRegion = ConfigurationManager.AppSettings["MinioRegion"];
            bool useSSL = bool.Parse(ConfigurationManager.AppSettings["UseSSL"]);
            var conexion = new MinioClient().WithEndpoint(minioEndpoint).WithCredentials(minioAccessKey, minioSecretKey).WithSSL(useSSL).WithRegion(minioRegion).Build();
            // Obtiene la metadata
            StatObjectArgs statObjectArgs = new StatObjectArgs()
                .WithBucket(bucketMatriculas)
                .WithObject(nombreArchivo);
            try
            {
                var metadata = await conexion.StatObjectAsync(statObjectArgs);
                return metadata.Size != 0;
            }
            catch (MinioException)
            {
                // Manejar el caso en que el archivo no existe
                return false;
            }
        }
        private async Task<ImagenesMatriculas> ObtenerImagenMatricula(long id)
        {
            ImagenesMatriculas imagenMatricula = null;
            try
            {
                using (SqlConnection conexion = new SqlConnection(conexionSIR))
                {
                    using (SqlCommand command = new SqlCommand("SP_ObtenerImagenMatricula", conexion))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@IdImagenMatricula", id);
                        await conexion.OpenAsync();
                        using (SqlDataReader dr = await command.ExecuteReaderAsync())
                        {
                            while (await dr.ReadAsync() && dr.HasRows)
                            {
                                imagenMatricula = new ImagenesMatriculas()
                                {
                                    Id = Convert.ToInt64(dr["Id"]),
                                    AltaFecha = Convert.ToDateTime(dr["AltaFecha"]),
                                    AltaUsuario = dr["AltaUsuario"] != DBNull.Value ? dr["AltaUsuario"].ToString() : null,
                                    ModificacionFecha = dr["ModificacionFecha"] != DBNull.Value ? (DateTime?)dr["ModificacionFecha"] : null,
                                    ModificacionUsuario = dr["ModificacionUsuario"] != DBNull.Value ? dr["ModificacionUsuario"].ToString() : null,
                                    BajaFecha = dr["BajaFecha"] != DBNull.Value ? (DateTime?)dr["BajaFecha"] : null,
                                    BajaUsuario = dr["BajaUsuario"] != DBNull.Value ? dr["BajaUsuario"].ToString() : null,
                                    Activo = Convert.ToBoolean(dr["Activo"]),
                                    Nombre = dr["Nombre"].ToString(),
                                    Ubicacion = dr["Ubicacion"].ToString(),
                                    Departamento = dr["Departamento"].ToString(),
                                    Matricula = dr["Matricula"].ToString(),
                                    Digito = dr["Digito"] != DBNull.Value ? (long?)dr["Digito"] : null,
                                    Duplicado = dr["Duplicado"] != DBNull.Value ? dr["Duplicado"].ToString() : null,
                                    Fecha = Convert.ToDateTime(dr["Fecha"]),
                                    Extension = dr["Extension"].ToString(),
                                    Size = dr["Size"].ToString(),
                                    Version = Convert.ToInt64(dr["Version"]),
                                };
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error al obtener el objeto fisico: " + ex.Message);
            }
            return imagenMatricula;
        }
        private Task<string> NuevoNombre(string departamento, string matricula, string digito, string duplicado)
        {
            if (string.IsNullOrWhiteSpace(digito)) digito = null;
            if (string.IsNullOrEmpty(duplicado)) duplicado = null;
            string nuevoNombre = departamento + '-' + matricula;
            if (!string.IsNullOrEmpty(digito) && !string.IsNullOrEmpty(duplicado)) nuevoNombre += '-' + digito + '-' + duplicado;
            if (!string.IsNullOrEmpty(digito) && string.IsNullOrEmpty(duplicado)) nuevoNombre += '-' + digito;
            if (string.IsNullOrEmpty(digito) && !string.IsNullOrEmpty(duplicado)) nuevoNombre += "--" + duplicado;
            nuevoNombre += ".pdf";
            return Task.FromResult(nuevoNombre);
        }
    }
}