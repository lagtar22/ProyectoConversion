﻿using ProyectoConversion.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web.Mvc;

namespace ProyectoConversion.Controllers
{
    [Authorize]
    public class DepartamentosController : Controller
    {
        private static readonly string conexion = ConfigurationManager.ConnectionStrings["AppDbContext"].ToString();
        private static List<Departamento> oDepartamentos = new List<Departamento>();
        // Vistas Generales
        public ActionResult Activos(string departamento)
        {
            oDepartamentos = new List<Departamento>();
            using (SqlConnection oConexion = new SqlConnection(conexion))
            {
                string query = "SELECT * FROM Departamentos WHERE Activo = 1 ";
                if (!string.IsNullOrEmpty(departamento))
                {
                    query += "AND Nombre LIKE @Busqueda OR Codigo LIKE @Busqueda ";
                }
                query += "ORDER BY Codigo";
                SqlCommand cmd = new SqlCommand(query, oConexion)
                {
                    CommandType = CommandType.Text
                };
                if (!string.IsNullOrEmpty(departamento))
                {
                    cmd.Parameters.AddWithValue("@Busqueda", "%" + departamento + "%");
                }
                oConexion.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Departamento nuevoDepartamento = new Departamento
                        {
                            Id = Convert.ToInt64(dr["Id"]),
                            AltaFecha = Convert.ToDateTime(dr["AltaFecha"]),
                            AltaUsuario = dr["AltaUsuario"] != DBNull.Value ? dr["AltaUsuario"].ToString() : null,
                            ModificacionFecha = dr["ModificacionFecha"] != DBNull.Value ? (DateTime?)dr["ModificacionFecha"] : null,
                            ModificacionUsuario = dr["ModificacionUsuario"] != DBNull.Value ? dr["ModificacionUsuario"].ToString() : null,
                            BajaFecha = dr["BajaFecha"] != DBNull.Value ? (DateTime?)dr["BajaFecha"] : null,
                            BajaUsuario = dr["BajaUsuario"] != DBNull.Value ? dr["BajaUsuario"].ToString() : null,
                            Activo = Convert.ToBoolean(dr["Activo"]),
                            Nombre = dr["Nombre"].ToString(),
                            Comentarios = dr["Comentarios"] != DBNull.Value ? dr["Comentarios"].ToString() : null,
                            UltimaMatricula = dr["UltimaMatricula"] != DBNull.Value ? (long?)dr["UltimaMatricula"] : null,
                            Codigo = dr["Codigo"] != DBNull.Value ? dr["Codigo"].ToString() : null
                        };
                        oDepartamentos.Add(nuevoDepartamento);
                    }
                }
            }
            ViewBag.SearchTerm = departamento;
            return View(oDepartamentos);
        }
        public ActionResult Eliminados()
        {
            oDepartamentos = new List<Departamento>();
            using (SqlConnection oConexion = new SqlConnection(conexion))
            {
                SqlCommand cmd = new SqlCommand("SELECT * FROM Departamentos WHERE Activo = 0 ORDER BY Codigo", oConexion)
                {
                    CommandType = CommandType.Text
                };
                oConexion.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Departamento nuevoDepartamento = new Departamento
                        {
                            Id = Convert.ToInt64(dr["Id"]),
                            AltaFecha = Convert.ToDateTime(dr["AltaFecha"]),
                            AltaUsuario = dr["AltaUsuario"] != DBNull.Value ? dr["AltaUsuario"].ToString() : null,
                            ModificacionFecha = dr["ModificacionFecha"] != DBNull.Value ? (DateTime?)dr["ModificacionFecha"] : null,
                            ModificacionUsuario = dr["ModificacionUsuario"] != DBNull.Value ? dr["ModificacionUsuario"].ToString() : null,
                            BajaFecha = dr["BajaFecha"] != DBNull.Value ? (DateTime?)dr["BajaFecha"] : null,
                            BajaUsuario = dr["BajaUsuario"] != DBNull.Value ? dr["BajaUsuario"].ToString() : null,
                            Activo = Convert.ToBoolean(dr["Activo"]),
                            Nombre = dr["Nombre"].ToString(),
                            Comentarios = dr["Comentarios"] != DBNull.Value ? dr["Comentarios"].ToString() : null,
                            UltimaMatricula = dr["UltimaMatricula"] != DBNull.Value ? (long?)dr["UltimaMatricula"] : null,
                            Codigo = dr["Codigo"] != DBNull.Value ? dr["Codigo"].ToString() : null
                        };
                        oDepartamentos.Add(nuevoDepartamento);
                    }
                }
            }
            return View(oDepartamentos);
        }
        // Redirección a Vistas especificas
        public ActionResult Insertar()
        { 
            return View();
        }
        public ActionResult Editar(long? idDpto)
        {
            if (idDpto == null) return RedirectToAction("Activos", "Departamentos");
            Departamento oDepto = oDepartamentos.Where(d => d.Id == idDpto).FirstOrDefault();
            return View(oDepto);
        }
        public ActionResult Eliminar(long? idDpto)
        {
            if (idDpto == null) return RedirectToAction("Activos", "Departamentos");
            Departamento oDepto = oDepartamentos.Where(d => d.Id == idDpto).FirstOrDefault();
            return View(oDepto);
        }
        public ActionResult Alta(long? idDpto)
        {
            if (idDpto == null) return RedirectToAction("Activos", "Departamentos");
            Departamento oDepto = oDepartamentos.Where(d => d.Id == idDpto).FirstOrDefault();
            return View(oDepto);
        }
        //Operaciones Métodos POST
        [HttpPost]
        public ActionResult Insertar(Departamento oDpto)
        {
            Usuario usuario = HttpContext.Session["Usuario"] as Usuario;
            if (!ModelState.IsValid)
            {
                if (string.IsNullOrEmpty(oDpto.Nombre)) { ModelState.AddModelError("Nombre", "El campo Nombre es obligatorio"); }
                if (!oDpto.UltimaMatricula.HasValue) { ModelState.AddModelError("UltimaMatricula", "El campo Última Matrícula es obligatorio"); }
                if (string.IsNullOrEmpty(oDpto.Codigo)) { ModelState.AddModelError("Codigo", "El campo Código es obligatorio"); }
                return View(oDpto);
            }
            oDpto.Nombre = oDpto.Nombre?.ToUpper();
            oDpto.Comentarios = oDpto.Nombre;
            oDpto.Codigo = oDpto.Codigo?.ToUpper();
            if (CodigoExistente(oDpto.Codigo))
            {
                TempData["FailMessage"] = "Código de Departamento ya existe.";
                return View(oDpto);
            }
            try
            {
                using (SqlConnection oConexion = new SqlConnection(conexion))
                {
                    SqlCommand cmd = new SqlCommand("SP_InsertarDepartamento", oConexion)
                    {
                        CommandType = CommandType.StoredProcedure,
                    };
                    cmd.Parameters.AddWithValue("AltaUsuario", usuario.User);
                    cmd.Parameters.AddWithValue("Nombre", oDpto.Nombre);
                    cmd.Parameters.AddWithValue("Comentarios", oDpto.Comentarios);
                    cmd.Parameters.AddWithValue("UltimaMatricula", oDpto.UltimaMatricula);
                    cmd.Parameters.AddWithValue("Codigo", oDpto.Codigo);
                    oConexion.Open();
                    cmd.ExecuteNonQuery();
                }
                TempData["SuccessMessage"] = "Departamento insertado exitosamente.";
                return RedirectToAction("Activos", "Departamentos");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Ocurrió un error al insertar el departamento: " + ex.Message);
                return View(oDpto);
            }
        }
        [HttpPost]
        public ActionResult Editar(Departamento oDpto)
        {
            Usuario usuario = HttpContext.Session["Usuario"] as Usuario;
            if (!ModelState.IsValid)
            {
                if (string.IsNullOrEmpty(oDpto.Nombre)) { ModelState.AddModelError("Nombre", "El campo Nombre es obligatorio"); }
                if (!oDpto.UltimaMatricula.HasValue) { ModelState.AddModelError("UltimaMatricula", "El campo Última Matrícula es obligatorio"); }
                if (string.IsNullOrEmpty(oDpto.Codigo)) { ModelState.AddModelError("Codigo", "El campo Código es obligatorio"); }
                return View(oDpto);
            }
            oDpto.Nombre = oDpto.Nombre?.ToUpper();
            oDpto.Comentarios = oDpto.Nombre;
            oDpto.Codigo = oDpto.Codigo?.ToUpper();
            if (CodigoExistente(oDpto.Codigo))
            {
                TempData["FailMessage"] = "Código de Departamento ya existe.";
                return View(oDpto);
            }
            try
            {
                using (SqlConnection oConexion = new SqlConnection(conexion))
                {
                    SqlCommand cmd = new SqlCommand("SP_ModificarDepartamento", oConexion)
                    {
                        CommandType = CommandType.StoredProcedure,
                    };
                    cmd.Parameters.AddWithValue("Id", oDpto.Id);
                    cmd.Parameters.AddWithValue("ModificacionUsuario", usuario.User);
                    cmd.Parameters.AddWithValue("Nombre", oDpto.Nombre);
                    cmd.Parameters.AddWithValue("Comentarios", oDpto.Comentarios);
                    cmd.Parameters.AddWithValue("UltimaMatricula", oDpto.UltimaMatricula);
                    cmd.Parameters.AddWithValue("Codigo", oDpto.Codigo);
                    oConexion.Open();
                    cmd.ExecuteNonQuery();
                }
                TempData["SuccessMessage"] = "Departamento editado exitosamente.";
                return RedirectToAction("Activos", "Departamentos");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Ocurrió un error al editar el departamento: " + ex.Message);
                return View(oDpto);
            }
        }
        [HttpPost]
        public ActionResult Eliminar(Departamento oDpto)
        {
            Usuario usuario = HttpContext.Session["Usuario"] as Usuario;
            try
            {
                using (SqlConnection oConexion = new SqlConnection(conexion))
                {
                    SqlCommand cmd = new SqlCommand("SP_BajaDepartamento", oConexion)
                    {
                        CommandType = CommandType.StoredProcedure,
                    };
                    cmd.Parameters.AddWithValue("Id", oDpto.Id);
                    cmd.Parameters.AddWithValue("BajaUsuario", usuario.User);
                    oConexion.Open();
                    cmd.ExecuteNonQuery();
                }
                TempData["SuccessMessage"] = "Departamento eliminado exitosamente.";
                return RedirectToAction("Eliminados", "Departamentos");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Ocurrió un error al eliminar el departamento: " + ex.Message);
                return View(oDpto);
            }
        }
        [HttpPost]
        public ActionResult Alta(Departamento oDpto)
        {
            Usuario usuario = HttpContext.Session["Usuario"] as Usuario;
            try
            {
                using (SqlConnection oConexion = new SqlConnection(conexion))
                {
                    SqlCommand cmd = new SqlCommand("SP_AltaDepartamento", oConexion)
                    {
                        CommandType = CommandType.StoredProcedure,
                    };
                    cmd.Parameters.AddWithValue("Id", oDpto.Id);
                    cmd.Parameters.AddWithValue("ModificacionUsuario", usuario.User);
                    oConexion.Open();
                    cmd.ExecuteNonQuery();
                }
                TempData["SuccessMessage"] = "Departamento dado de alta exitosamente.";
                return RedirectToAction("Activos", "Departamentos");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Ocurrió un error al dar de alta el departamento: " + ex.Message);
                return View(oDpto);
            }
        }
        [HttpPost]
        public ActionResult Buscar(string departamento)
        {
            return RedirectToAction("Activos", new { departamento });
        }
        private bool CodigoExistente(string pCodigo)
        {
            // Verificar si el código ya existe en la lista de departamentos
            return oDepartamentos.Any(depto => depto.Codigo.Equals(pCodigo, StringComparison.OrdinalIgnoreCase));
        }
    }
}