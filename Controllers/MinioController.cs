﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using System.Web.Mvc;
using Minio;
using Minio.DataModel.Args;
using Minio.Exceptions;
using ProyectoConversion.Models;

namespace ProyectoConversion.Controllers
{
    [Authorize]
    public class MinioController : Controller
    {
        private readonly string minioEndpoint;
        private readonly string minioAccessKey;
        private readonly string minioSecretKey;
        private readonly string minioRegion;
        private readonly bool useSSL;
        private readonly string bucketMatriculas;
        private readonly string bucketHistoricos;
        private static readonly string conexionSIR = ConfigurationManager.ConnectionStrings["AppDbContext"].ToString();
        public MinioController()
        {
            minioEndpoint = ConfigurationManager.AppSettings["MinioEndpoint"];
            minioAccessKey = ConfigurationManager.AppSettings["MinioAccessKey"];
            minioSecretKey = ConfigurationManager.AppSettings["MinioSecretKey"];
            minioRegion = ConfigurationManager.AppSettings["MinioRegion"];
            useSSL = bool.Parse(ConfigurationManager.AppSettings["UseSSL"]);
            bucketMatriculas = ConfigurationManager.AppSettings["BucketMatriculasVigentes"];
            bucketHistoricos = ConfigurationManager.AppSettings["BucketMatriculasHistoricas"];
        }
        public async Task<JsonResult> Visualizar(long? idImgMat, long versionArchivo)
        {
            if (idImgMat == null)
            {
                TempData["error"] = "ID de Matrícula nulo.";
                return Json(new { existencia = false}, JsonRequestBehavior.AllowGet);
            }
            ImagenesMatriculas nuevaImagen = await ObtenerImagenMatriculaPorId((long)idImgMat);
            if (nuevaImagen == null)
            {
                TempData["error"] = "La información de la Matrícula es nula.";
                return Json(new { existencia = false}, JsonRequestBehavior.AllowGet);
            }
            var minio = new MinioClient().WithEndpoint(minioEndpoint).WithCredentials(minioAccessKey, minioSecretKey).WithSSL(useSSL).WithRegion(minioRegion).Build();
            // Verificar si el archivo existe en Minio
            string bucket = bucketMatriculas;
            bool archivoExiste = await BuscarArchivoMinio(minio, nuevaImagen, bucket);
            if (!archivoExiste)
            {
                TempData["error"] = "El archivo no existe en Minio. (V)";
                return Json(new { existencia = false}, JsonRequestBehavior.AllowGet);
            }
            try
            {
                // Devolver información sobre la existencia del archivo y el enlace directo al archivo
                return Json(new { existencia = true, archivoUrl = Url.Action("Descargar", "Minio", new { idImgMat, versionArchivo, bandera = true }) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Manejar cualquier error que pueda ocurrir al visualizar el archivo
                Console.WriteLine($"Error al visualizar el archivo: {ex.Message}");
                return Json(new { existencia = false, mensaje = $"Error al visualizar el archivo: {ex.Message}" }, JsonRequestBehavior.AllowGet);
            }
        }
        public async Task<ActionResult> Descargar(long? idImgMat, long versionArchivo, bool? bandera)
        {
            bool flag = bandera ?? false;
            if (idImgMat == null)
            {
                TempData["error"] = "ID de Matrícula nulo.";
                return RedirectToAction("Activos", "ImagenesMatriculas");
            }
            try
            {                
                ImagenesMatriculas imagenMatricula = await ObtenerImagenMatriculaPorId(idImgMat.Value);                
                if (imagenMatricula == null)
                {
                    TempData["error"] = "La información de la Matrícula es nula.";
                    return RedirectToAction("Activos", "ImagenesMatriculas");
                }
                string bucket = bucketMatriculas;
                if (versionArchivo != imagenMatricula.Version)
                {
                    imagenMatricula.Nombre = await NuevoNombre(imagenMatricula.Departamento, imagenMatricula.Matricula, imagenMatricula.Digito.ToString(), imagenMatricula.Duplicado, versionArchivo.ToString());
                    bucket = bucketHistoricos;
                }
                // Configurar la conexión Minio
                var minio = new MinioClient().WithEndpoint(minioEndpoint).WithCredentials(minioAccessKey, minioSecretKey).WithSSL(useSSL).WithRegion(minioRegion).Build();
                // Verificar si el archivo existe en Minio
                bool archivoExiste = await BuscarArchivoMinio(minio, imagenMatricula, bucket);
                if (!archivoExiste)
                {
                    TempData["error"] = "El archivo no existe en Minio. (D)";
                    return RedirectToAction("Activos", "ImagenesMatriculas");
                }
                // Leer el contenido del archivo desde Minio
                byte[] archivoBytes = await LeerArchivoMinio(minio, imagenMatricula, bucket);
                // Devolver el archivo para su descarga
                if (flag) // Si la bandera es verdadera, muestra el contenido en línea
                {                    
                    Response.Headers.Add("Content-Disposition", $"inline; filename=\"{imagenMatricula.Nombre}\"");
                }
                else // Si la bandera es falsa o no se proporciona, descarga el archivo
                {                    
                    Response.Headers.Add("Content-Disposition", $"attachment; filename=\"{imagenMatricula.Nombre}\"");
                }
                return File(archivoBytes, "application/pdf");
            }
            catch (Exception ex)
            {
                // Manejar cualquier error que pueda ocurrir al descargar el archivo
                Console.WriteLine($"Error al descargar el archivo: {ex.Message}");
                TempData["error"] = $"Error al descargar el archivo: {ex.Message}";
                return RedirectToAction("Activos", "ImagenesMatriculas");
            }
        }
        public async Task<ActionResult> Subir()
        {
            ArchivoImagen nuevoArchivo = TempData["ArchivoImagen"] as ArchivoImagen;
            if (nuevoArchivo.ImagenMatricula == null || nuevoArchivo.ArchivoPdf == null)
            {
                TempData["error"] = "La información de la Matrícula es nula.";
                return RedirectToAction("Precarga", "ImagenesMatriculas");
            }
            ArchivoPdf archivoSubir = nuevoArchivo.ArchivoPdf;
            try
            {
                string nombre = archivoSubir.NombreArchivo;
                // Create a PutObjectArgs instance
                var putObjectArgs = new PutObjectArgs()
                    .WithStreamData(new MemoryStream(archivoSubir.Contenido))
                    .WithObjectSize(archivoSubir.Contenido.Length)
                    .WithBucket(bucketMatriculas)
                    .WithObject(nombre);
                // Configuración de conexión a Minio
                var minio = new MinioClient().WithEndpoint(minioEndpoint).WithCredentials(minioAccessKey, minioSecretKey).WithSSL(useSSL).WithRegion(minioRegion).Build();
                // Subida del archivo
                var putObjectResponse = await minio.PutObjectAsync(putObjectArgs);
                TempData["exito"] = "Archivo subido con éxito.";
                return RedirectToAction("Activos", "ImagenesMatriculas");
            }
            catch (Exception ex)
            {
                TempData["error"] = $"Error al tratar de subir el archivo a minio: {ex.Message}";
                return RedirectToAction("Activos", "ImagenesMatriculas");
            }
        }
        public async Task<ActionResult> Editar()
        {
            ArchivoImagen nuevoArchivo = TempData["ArchivoImagen"] as ArchivoImagen;
            if (nuevoArchivo.ImagenMatricula == null | nuevoArchivo.ArchivoPdf == null)
            {

                return RedirectToAction("Precarga", "ImagenesMatriculas");
            }
            ArchivoPdf archivoNuevo = nuevoArchivo.ArchivoPdf;
            ImagenesMatriculas imagenRegistro = nuevoArchivo.ImagenMatricula;
            string nombreArchivoViejo = await NuevoNombre(imagenRegistro.Departamento, imagenRegistro.Matricula, imagenRegistro.Digito.ToString(), imagenRegistro.Duplicado, imagenRegistro.Version.ToString());
            try
            {
                var minio = new MinioClient().WithEndpoint(minioEndpoint).WithCredentials(minioAccessKey, minioSecretKey).WithSSL(useSSL).WithRegion(minioRegion).Build();
                ArchivoPdf archivoViejo = new ArchivoPdf
                {
                    NombreArchivo = nombreArchivoViejo,
                    Contenido = await LeerArchivoMinio(minio, imagenRegistro, bucketMatriculas),
                };
                string nombreNuevo = archivoNuevo.NombreArchivo;
                //subida de archivo viejo a bucket historico
                var putObjectArgsViejo = new PutObjectArgs()
                                            .WithStreamData(new MemoryStream(archivoViejo.Contenido))
                                            .WithObjectSize(archivoViejo.Contenido.Length)
                                            .WithBucket(bucketHistoricos)
                                            .WithObject(archivoViejo.NombreArchivo);
                await minio.PutObjectAsync(putObjectArgsViejo);
                //elimiar archivo viejo en bucket matriculas
                var removeObjectArgs = new RemoveObjectArgs()
                                            .WithBucket(bucketMatriculas)
                                            .WithObject(archivoNuevo.NombreArchivo);
                await minio.RemoveObjectAsync(removeObjectArgs);
                //subida archivo nuevo a bucket matriculas
                var putObjectArgsNuevo= new PutObjectArgs()
                                            .WithStreamData(new MemoryStream(archivoNuevo.Contenido))
                                            .WithObjectSize(archivoNuevo.Contenido.Length)
                                            .WithBucket(bucketMatriculas)
                                            .WithObject(archivoNuevo.NombreArchivo);
                await minio.PutObjectAsync(putObjectArgsNuevo);
                TempData["exito"] = "Archivo y registro editados con éxito!";
                return RedirectToAction("Activos", "ImagenesMatriculas");
            }
            catch (Exception ex)
            {
                TempData["error"] = $"Error al tratar de subir el archivo a minio: {ex.Message}";
                return RedirectToAction("Activos", "ImagenesMatriculas");
            }
        }
        private async Task<bool> BuscarArchivoMinio(IMinioClient conexion, ImagenesMatriculas imagen, string bucket)
        {
            // Obtiene la metadata
            StatObjectArgs statObjectArgs = new StatObjectArgs()
                .WithBucket(bucket)
                .WithObject(imagen.Nombre);
            try
            {
                var metadata = await conexion.StatObjectAsync(statObjectArgs);
                return metadata.Size != 0;
            }
            catch (MinioException)
            {
                return false;
            }
        }
        private async Task<byte[]> LeerArchivoMinio(IMinioClient conexion, ImagenesMatriculas imagen, string bucket)
        {
            MemoryStream memoryStream = new MemoryStream();
            GetObjectArgs getObjectArgs = new GetObjectArgs()
                .WithBucket(bucket)
                .WithObject(imagen.Nombre)
                .WithCallbackStream((stream) =>
                {
                    stream.CopyTo(memoryStream);
                });
            await conexion.GetObjectAsync(getObjectArgs);
            byte[] archivoBytes = memoryStream.ToArray();
            return archivoBytes;
        }
        private async Task<ImagenesMatriculas> ObtenerImagenMatriculaPorId(long id)
        {
            ImagenesMatriculas oImagenMatricula = new ImagenesMatriculas();
            try
            {
                using (SqlConnection oConexion = new SqlConnection(conexionSIR))
                {
                    using (SqlCommand command = new SqlCommand("SP_ObtenerImagenMatricula", oConexion))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@IdImagenMatricula", id);
                        await oConexion.OpenAsync();
                        using (SqlDataReader dr = await command.ExecuteReaderAsync())
                        {
                            while (await dr.ReadAsync() && dr.HasRows)
                            {
                                oImagenMatricula = new ImagenesMatriculas
                                {
                                    Id = Convert.ToInt64(dr["Id"]),
                                    AltaFecha = Convert.ToDateTime(dr["AltaFecha"]),
                                    AltaUsuario = dr["AltaUsuario"] != DBNull.Value ? dr["AltaUsuario"].ToString() : null,
                                    ModificacionFecha = dr["ModificacionFecha"] != DBNull.Value ? (DateTime?)dr["ModificacionFecha"] : null,
                                    ModificacionUsuario = dr["ModificacionUsuario"] != DBNull.Value ? dr["ModificacionUsuario"].ToString() : null,
                                    BajaFecha = dr["BajaFecha"] != DBNull.Value ? (DateTime?)dr["BajaFecha"] : null,
                                    BajaUsuario = dr["BajaUsuario"] != DBNull.Value ? dr["BajaUsuario"].ToString() : null,
                                    Activo = Convert.ToBoolean(dr["Activo"]),
                                    Nombre = dr["Nombre"].ToString(),
                                    Ubicacion = dr["Ubicacion"].ToString(),
                                    Departamento = dr["Departamento"].ToString(),
                                    Matricula = dr["Matricula"].ToString(),
                                    Digito = dr["Digito"] != DBNull.Value ? (long?)dr["Digito"] : null,
                                    Duplicado = dr["Duplicado"] != DBNull.Value ? dr["Duplicado"].ToString() : null,
                                    Fecha = Convert.ToDateTime(dr["Fecha"]),
                                    Extension = dr["Extension"].ToString(),
                                    Size = dr["Size"].ToString(),
                                    Version = Convert.ToInt64(dr["Version"]),
                                };
                            }
                        }
                    }
                }
                return oImagenMatricula;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error al obtener la imagen de matrícula: " + ex.Message);
                return null;
            }
        }
        private Task<string> NuevoNombre(string departamento, string matricula, string digito, string duplicado, string version)
        {
            if (string.IsNullOrWhiteSpace(digito)) digito = null;
            if (string.IsNullOrEmpty(duplicado)) duplicado = null;
            string nuevoNombre = departamento + '-' + matricula;
            if (!string.IsNullOrEmpty(digito) && !string.IsNullOrEmpty(duplicado)) nuevoNombre += '-' + digito + '-' + duplicado;
            if (!string.IsNullOrEmpty(digito) && string.IsNullOrEmpty(duplicado)) nuevoNombre += '-' + digito;
            if (string.IsNullOrEmpty(digito) && !string.IsNullOrEmpty(duplicado)) nuevoNombre += "--" + duplicado;
            if (!string.IsNullOrEmpty(version)) nuevoNombre += "(" + version + ")";
            nuevoNombre += ".pdf";
            return Task.FromResult(nuevoNombre);
        }
    }
}