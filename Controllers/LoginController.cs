﻿using ProyectoConversion.Models;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Web.Mvc;
using System.Web.Security;
using System.Security.Cryptography;

namespace ProyectoConversion.Controllers
{
    public class LoginController : Controller
    {
        private static readonly string conexion = ConfigurationManager.ConnectionStrings["KcDbContext"].ToString();
        // GET: Login
        public ActionResult Login() //devuelve la vista de login
        {
            return View();
        }
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login","Login");
        }

        [HttpPost]
        public ActionResult Login(string pUser, string pPass)
        {
            if (ValidateUser(pUser, pPass))
            {
                Usuario usuario = new Usuario
                {
                    User = pUser
                };
                FormsAuthentication.SetAuthCookie(usuario.User, false);
                Session["Usuario"] = usuario;
                // Usuario válido, redirigir al inicio
                return RedirectToAction("Inicio", "Inicio");
            }
            else
            {
                // Usuario inválido, mostrar mensaje de error en la vista de login
                ViewBag.ErrorMessage = "Usuario o contraseña incorrectos";
                Session["Usuario"] = null;
                return View("Login");
            }
        }
        private bool ValidateUser(string username, string password)
        {
            // Obtener salt del usuario
            string salt = GetSalt(username);
            if (!string.IsNullOrEmpty(salt))
            {
                // Encriptar contraseña ingresada con la salt del usuario
                string hashedPassword = HashPassword(password, Convert.FromBase64String(salt));
                // Validar contraseña en la base de datos
                return ValidatePasswordInDatabase(username, hashedPassword);
            }
            return false;
        }
        private string GetSalt(string username)
        {
            string salt = "";
            try
            {
                using (SqlConnection connection = new SqlConnection(conexion))
                {
                    using (SqlCommand command = new SqlCommand("SP_ObtenerSalt", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Username", username);
                        connection.Open();
                        var result = command.ExecuteScalar();
                        if (result != null && result != DBNull.Value)
                        {
                            salt = result.ToString(); // Asignar la salt obtenida del resultado
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Manejar la excepción según tu lógica, por ejemplo, registrar el error
                // en algún archivo de registro o mostrar un mensaje de error genérico
                Console.WriteLine("Error al obtener la salt del usuario: " + ex.Message);
            }
            return salt;
        }
        private bool ValidatePasswordInDatabase(string username, string hashedPassword)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["KcDbContext"].ConnectionString;
            bool isValidPassword = false;
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("SP_ValidarContrasena", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Username", username);
                        command.Parameters.AddWithValue("@HashedPassword", hashedPassword);

                        connection.Open();
                        var result = command.ExecuteScalar();

                        if (result != null && result != DBNull.Value && Convert.ToBoolean(result))
                        {
                            isValidPassword = true; // La contraseña coincide
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Manejar la excepción según tu lógica, por ejemplo, registrar el error
                // en algún archivo de registro o mostrar un mensaje de error genérico
                Console.WriteLine("Error al validar la contraseña en la base de datos: " + ex.Message);
            }
            return isValidPassword;
        }
        private string HashPassword(string password, byte[] salt)
        {
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 27500, HashAlgorithmName.SHA256);
            byte[] hash = pbkdf2.GetBytes(64);

            // Convertir el hash a base64
            return Convert.ToBase64String(hash);
        }
    }
}